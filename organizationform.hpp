#pragma once

#include <QDialog>

namespace Ui{
	class Dialog;
}

class Organization : public QDialog
{
	Q_OBJECT
public:
	explicit Organization(QWidget *parent = 0);
	~Organization();

	QString getName() { return name; }
	QString getEmail() { return email; }
	bool to_save() { return save; }
private slots:
	void accept();
private:
	Ui::Dialog *ui;

	QString name;
	QString email;
	bool save;
};