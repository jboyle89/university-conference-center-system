##University Conference Center System

# To build:

* Uses Cmake >= 3.0

* Set "CMAKE_PREFIX_PATH=<your_Qt5_install_dir>"

* Update lines 9-12 of mainwindow.cpp to the proper database info

#Requires:

* Qt5

#DLLs:

* icudt53

* icuin53

* icuuc53

* Qt5Core

* Qt5Gui

* Qt5Widgets

* Qt5Sql