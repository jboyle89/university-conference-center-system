#include "organizationform.hpp"
#include "ui_organizationform.h"

#include <QtSql>

Organization::Organization(QWidget *parent) :
QDialog(parent),
ui(new Ui::Dialog),
save(false)
{
	ui->setupUi(this);
	connect(ui->buttonBox, SIGNAL(ui->buttonBox->accepted()), this, SLOT(accept()));
}

void Organization::accept()
{
	name = ui->nameEdit->text();
	email = ui->emailEdit->text();
	save = true;

	this->close();
}

Organization::~Organization()
{
	delete ui;
}