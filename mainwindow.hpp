#pragma once

#include <QMainWindow>
#include <QtSql>

namespace Ui{
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
private slots:
	void refresh();
	void add_org();
	void add_conf();
private:
	Ui::MainWindow *ui;
	QSqlDatabase db;
};