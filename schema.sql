CREATE TABLE rooms (
        id INTEGER NOT NULL AUTO_INCREMENT,
        name VARCHAR(100),
        cost INTEGER,
        PRIMARY KEY ( id )
);
CREATE TABLE organizations (
        id INTEGER NOT NULL AUTO_INCREMENT,
        name VARCHAR(100),
        contact_email VARCHAR(100),
        PRIMARY KEY ( id )
);
CREATE TABLE conferences (
        id INTEGER NOT NULL AUTO_INCREMENT,
        organization_id INTEGER,
        name VARCHAR(100),
        PRIMARY KEY ( id ),
        FOREIGN KEY(organization_id) REFERENCES organizations (id)
);
CREATE TABLE attendees (
        id INTEGER NOT NULL AUTO_INCREMENT,
        first_name VARCHAR(100),
        last_name VARCHAR(100),
        conference_id INTEGER,
        PRIMARY KEY ( id ),
        FOREIGN KEY(conference_id) REFERENCES conferences (id)
);
CREATE TABLE events (
        id INTEGER NOT NULL AUTO_INCREMENT,
        conference_id INTEGER,
        room_id INTEGER,
        name VARCHAR(100),
        date_of DATE NOT NULL,
        start_time TIME NOT NULL,
        end_time TIME NOT NULL,
        special_event BOOLEAN,
        PRIMARY KEY (id),
        FOREIGN KEY(conference_id) REFERENCES conferences (id),
        FOREIGN KEY(room_id) REFERENCES rooms (id),
        CHECK (special_event IN (0, 1))
);
CREATE TABLE special_event_registry (
        id INTEGER NOT NULL AUTO_INCREMENT,
        attendee INTEGER,
        event INTEGER,
        paid BOOLEAN,
        PRIMARY KEY (id),
        FOREIGN KEY(attendee) REFERENCES attendees (id),
        FOREIGN KEY(event) REFERENCES events (id),
        CHECK (paid IN (0, 1))
);
CREATE TABLE equipment (
        id INTEGER NOT NULL AUTO_INCREMENT,
        type VARCHAR(100),
        status INTEGER,
        event_id INTEGER,
        PRIMARY KEY (id),
        FOREIGN KEY(event_id) REFERENCES events (id)
);