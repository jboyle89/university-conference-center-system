#pragma once

#include <QDialog>

namespace Ui {
	class ConferenceDialog;
}

class Conference : public QDialog
{
	Q_OBJECT
public:
	Conference(QWidget *parent = 0);
	~Conference();

	QString getName() { return name; }
	int getOrg() { return org_id; }
	bool to_save() { return save; }
private slots:
	void accept();
private:
	Ui::ConferenceDialog *ui;

	QString name;
	int org_id;
	bool save;
};