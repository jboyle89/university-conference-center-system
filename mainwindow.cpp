#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "organizationform.hpp"
#include "conferenceform.hpp"

MainWindow::MainWindow(QWidget *parent):
QMainWindow(parent),
ui(new Ui::MainWindow)
{
	db = QSqlDatabase::addDatabase("QMYSQL");
	db.setHostName("127.0.0.1");
	db.setDatabaseName("UCCS");
	db.setUserName("username");
	db.setPassword("password");
	if (!db.open())
		qDebug() << db.lastError();

	ui->setupUi(this);
	ui->tableView->setSortingEnabled(true);
	ui->dateEdit->setDate(QDate::currentDate());

	connect(ui->refreshButton, &QPushButton::clicked, this, &MainWindow::refresh);
	connect(ui->dateEdit, &QDateEdit::dateChanged, this, &MainWindow::refresh);
	connect(ui->actionOrganization, SIGNAL(triggered()), this, SLOT(add_org()));
	connect(ui->actionConference, SIGNAL(triggered()), this, SLOT(add_conf()));

	this->refresh();
}

void MainWindow::refresh()
{
	//Query events for the main schedule view
	QSqlQueryModel *model = new QSqlQueryModel;
	QSqlQuery query("SELECT t1.name, t2.name, t4.name, t3.name, t1.date_of, t1.start_time, t1.end_time "
		"FROM events AS t1 INNER JOIN conferences AS t2 ON t1.conference_id = t2.id "
		"INNER JOIN rooms AS t3 ON t1.room_id = t3.id INNER JOIN organizations AS t4 on t2.organization_id = t4.id WHERE t1.date_of = ?", db);
	query.addBindValue(QVariant(ui->dateEdit->date()));
	query.exec();
	model->setQuery(query);
	model->setHeaderData(0, Qt::Horizontal, "Event");
	model->setHeaderData(1, Qt::Horizontal, "Conference");
	model->setHeaderData(2, Qt::Horizontal, "Organization");
	model->setHeaderData(3, Qt::Horizontal, "Room");
	model->setHeaderData(4, Qt::Horizontal, "Date");
	model->setHeaderData(5, Qt::Horizontal, "Start Time");
	model->setHeaderData(6, Qt::Horizontal, "End Time");

	//Make event model sortable
	QSortFilterProxyModel *proxy = new QSortFilterProxyModel;
	proxy->setSourceModel(model);

	//Connect event model to table view
	ui->tableView->setModel(proxy);
}

void MainWindow::add_org()
{
	Organization o;
	o.exec();

	if (o.to_save())
	{
		QSqlQuery query("INSERT INTO organizations(name, contact_email) VALUES(?,?)", db);
		query.addBindValue(o.getName());
		query.addBindValue(o.getEmail());

		query.exec();
	}
}

void MainWindow::add_conf()
{
	Conference c;
	c.exec();

	if (c.to_save())
	{
		QSqlQuery query("INSERT INTO conferences(name, organization_id) VALUES(?,?)", db);
		query.addBindValue(c.getName());
		query.addBindValue(c.getOrg());

		query.exec();
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}