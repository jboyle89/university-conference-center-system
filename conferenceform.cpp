#include "conferenceform.hpp"
#include "ui_conferenceform.h"

#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlResult>
#include <QDebug>

Conference::Conference(QWidget *parent):
QDialog(parent),
ui(new Ui::ConferenceDialog),
save(false)
{
	ui->setupUi(this);

	QSqlQuery q("SELECT name FROM organizations");
	q.exec();

	QSqlQueryModel *model = new QSqlQueryModel;
	model->setQuery(q);

	ui->comboBox->setModel(model);
	ui->comboBox->setModelColumn(0);
}

Conference::~Conference()
{
	delete ui;
}

void Conference::accept()
{
	name = ui->nameEdit->text();
	org_id = ui->comboBox->currentIndex() + 1;
	save = true;

	this->close();
}